# Getting Started

This guide will help you setup your project and configure all the tools required to participate in this hackathon.

!!! warning
    You must [create a GitLab account](https://gitlab.com/users/sign_up) (_on gitlab.com_) and send your username or email to the Hackathon administrator.

## Technical requirements

Before you start, make sure you meet all the requirements.

* **Git** | [:octicons-gear-16: installation](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) | [:material-file-document-outline: cheatsheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
* **Python (>= 3.9)** | [:octicons-gear-16: installation](https://www.python.org/downloads/) 
* **DVC** | [:octicons-gear-16: installation](https://dvc.org/doc/install) | [:octicons-video-16: tutorial](https://www.youtube.com/watch?v=kLKBcPonMYw) | [:material-file-document-outline: cheatsheet](https://i1.wp.com/www.globalsqa.com/wp-content/uploads/2020/05/DVC_cheatsheet.png?w=1880&ssl=1)

We use [DVC :octicons-link-external-16:](https://dvc.org/) for handling ML models that, due to their large file size, cannot be pushed to GitLab.

## Installation

First you need to [:one: create your project](tutorials/install.md), then you can [:two: make your first submission](tutorials/submission.md).

## Subjects

[:material-file-document-outline: Sentiment Analysis](subjects/sentiment-analysis.md) - Binary classification of sentiment on movie reviews
