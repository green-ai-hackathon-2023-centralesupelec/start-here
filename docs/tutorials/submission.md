# Make a submission

You have implemented a new solution for the hackathon and want to send a new submission to test it.

**Check list before submitting:**

- [x] [Have you uploaded your model using DVC?](dvc.md)
- [x] Have updated your `evaluation.py` script according to your model? 
- [x] Have you added your python libraries into the `requirements.txt` file?

??? note "Is it your first submission?"
    When proceeding with the first submission you do not need to upload a model or change the evaluation script. The first submission will just predict random outputs (depending on the subject). This is solely to test that everything works well. 

## Start a submission

You can start a new submission right from your project repository on GitLab. You can click on the :fast_forward: button from your last commit, and the click :arrow_forward: on the "submission-job" button. Finally, to watch the logs of the job you can click on the "submission-job" button.

**Click :fast_forward: on the commit:**

![Commit screenshot](../img/tutorials/commit.png){ width="500" }

**Click :arrow_forward: on the job:**

![Commit screenshot](../img/tutorials/job.png){ width="200" }

If you are using multiple branches be sure to switch branch to see your last commit on the right branch and then launch the submission.

When the submission succeeded, you should see job metrics logged at the end, like the following:

![Job metrics](../img/tutorials/job_metrics.png){ width="500" }

## Troubleshooting

If the job execution failed, take a look at the logs to try debugging it. If the issue is not coming from your code, don't hesitate to contact the staff to get some help.

## Access past submissions

Navigate in the **:octicons-rocket-24: Build > Jobs** to see past submissions.

## Speed-up submissions

You can use pre-built images for CI pipelines that comes with ML libraries pre-installed and ready to use with a GPU. To proceed you can change the `image` in your `.gitlab-ci.yml` file. 

```yaml
default:
  image: registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-ci:latest
```

**List of available images:**


=== "Python 3.10"
    Base:
    ```bash
    registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-ci:latest
    ```

    Pytorch 2.1+ (GPU):
    ```bash
    registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-torch2.1-cu121-ci
    ```

    Tensorflow 2.14+ (GPU):
    ```bash
    registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.10-slim-tensorflow2.14-cu121-ci
    ```

=== "Python 3.9"
    Base:
    ```bash
    registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-ci:latest
    ```

    Pytorch 2.1+ (GPU):
    ```bash
    registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-torch2.1-cu121-ci:latest
    ```

    Tensorflow 2.14+ (GPU):
    ```bash
    registry.gitlab.com/green-ai-hack/core/base-runner-image/python3.9-slim-tensorflow2.14-cu121-ci:latest
    ```

If you have specific needs don't hesitate to ask the staff.
