# DVC

**DVC** stands for Data Version Control, it's a software that helps data scientists to keep track of AI models and datasets versions in a project by using a git-like interface. AI models and datasets are stored on an external system, but tracked within the git repository with `*.dvc` files.

## Installation

=== "Windows"
    If you have `choco` ([Chocolatey :octicons-link-external-16:](https://chocolatey.org/)) installed it is recommended to install DVC with:
    ```bash
    choco install dvc
    ```
    Or follow the instructions [here :octicons-link-external-16:](https://dvc.org/doc/install/windows).

=== "macOS"
    If you have `brew` ([Homebrew :octicons-link-external-16:](https://brew.sh/)) installed it is recommended to install DVC with:
    ```bash
    brew install dvc
    ```
    Or follow the instructions [here :octicons-link-external-16:](https://dvc.org/doc/install/macos).

=== "Linux"
    Follow the instructions [here :octicons-link-external-16:](https://dvc.org/doc/install/linux)

## Usage

To get an overview of how DVC works you can watch: [:octicons-video-16: Getting started with DVC](https://www.youtube.com/watch?v=kLKBcPonMYw)

### Add a new model

Let's say that you have trained a new model and want to use for a new submission.

Example project structure:

```
project
└── models
    └── bert
        └── model.pt
```

You will need to upload your model weights (`model.pt` here) using DVC.

```bash
dvc add models/bert/model.pt
dvc push
```

Then add, commit and push the new `.dvc` file, you just created. :warning: Do not forget to update your `evaluation.py` script before doing a new submission.

```bash
git add models/bert/model.pt.dvc
git commit -m "add bert model"
git push
```

### Download all models

To download all models from one repository, you can simply `pull` them:

```bash
dvc pull
```
