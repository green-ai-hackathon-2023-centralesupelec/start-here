# Create your project

Follow this guide to set up your project for this hackathon.

!!! warning
    **Only one team member should create and configure the DVC remote.** All team members must do the final step "Authentication for DVC."

### Fork project template

You will need to fork the repository named "Project Template". If you are logged into GitLab, you can click the button below.

!!! warning
    1. You must pay attention that you fork the project to right destination (green-ai-hack > hackathon > **SUBGROUP**). Select the according "**Project URL**".
    2. Take note of your project slug, you will need later to set up DVC.

<p>
    <a class="md-button" href="https://gitlab.com/green-ai-hack/project-template/-/forks/new" target="_blank">Click to fork Project Template</a>
</p>

![fork screenshot](../img/tutorials/fork.png)

### Clone your project

```bash
git clone <PROJECT_URL>
```

??? info "How to get your project URL?"
    You can get the project URL to clone by HTTPS (recommended) or SSH on your project's page under the "Clone" button.
    ![clone screenshot](../img/tutorials/clone.png)

### Configure DVC remote

We will change the remote URL used by DVC to match your project. Replace `<PROJECT_SLUG>` with your own project slug.

```bash
dvc remote modify hackathon-remote url s3://gah-model-s3/<PROJECT_SLUG>
```

Push the changes onto the remote git repository

```bash
git add .dvc/config
git commit -m "chore: update dvc url"
git push
```

### Authentication for DVC

Set up authentication for DVC using your own credentials. :warning: Must be done by all team members after they cloned the project.

```bash
dvc remote modify --local hackathon-remote access_key_id <ACCESS_KEY_ID>
dvc remote modify --local hackathon-remote secret_access_key <SECRET_ACCESS_KEY>
```
