# GitLab Account

Prior to the online registration to the Green AI Hack, **you will need a GitLab account** because you will be prompted to fill in your GitLab username. **This is mandatory to be added to the GitLab team that will used during the event.**

## Create a GitLab account

!!! warning "Your GitLab account must be created on the gitlab.com instance."
    
    You will need an account on the public instance of GitLab hosted on gitlab.com. Having an account on any other GitLab instance will not work.    

If you already have an account you can skip this step. Otherwise, please [sign up for a new GitLab account :octicons-link-external-16:](https://gitlab.com/users/sign_up).

## Get your GitLab username

To get your GitLab username, [sign in on gitlab.com :octicons-link-external-16:](https://gitlab.com/users/sign_in). Then click on your profile in the top left corner, there you will find your account username that starts with an `@` symbol.

![Get GitLab username](../img/tutorials/gitlab_account.png){ width="400" }
