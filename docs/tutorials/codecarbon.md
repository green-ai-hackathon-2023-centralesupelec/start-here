# CodeCarbon

**CodeCarbon** is a Python library that tracks the energy consumption and carbon footprint while running your code. It works on almost any system and account for CPU, RAM and GPU energy consumption.

## Installation

```shell
pip install codecarbon
```

For more installation options, please refer to the [official installation instructions :octicons-link-external-16:](https://mlco2.github.io/codecarbon/installation.html).

## Usage

You can track the energy consumption of your code by following this example. After each run, a new line will be appended to the `emissions.csv` file.

```python
from codecarbon import EmissionsTracker

tracker = EmissionsTracker()

tracker.start()
# Your compute intensive code goes here
tracker.stop()
```

To learn more on the possible configurations and usage of CodeCarbon, please refer to the [official documentation :octicons-link-external-16:](https://mlco2.github.io/codecarbon/usage.html).

!!! note "Variability of the results"

    **Running the same code multiple times in a row can yield different results in terms of energy consumption and carbon footprint.** This is mainly caused by all the other processes that runs in parallel on your system. To get more accurate results, try to limit the number of programs and intensive computation that runs on your system.
