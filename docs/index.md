
<style>
h1 {display: none;}
</style>

<div align="center">
  <p class="font-logo">Green AI Hack<i class="blink-logo">_</i></p>
</div>

Welcome to the **Green AI Hack**! :tada:


We're thrilled to have you on board for this exciting event that puts your Machine Learning skills to the test. Throughout the hackathon, your objective is to craft a top-notch model that not only demonstrates **solid accuracy** but also prioritizes **minimal environmental impacts**. Get ready for a unique challenge that combines :robot: innovation with :earth_africa: sustainability!


This hackathon is made possible by the hard work of volunteers who built the supporting tool stack, with special thanks to [SDIA](https://sdialliance.org/), [Boavizta](https://boavizta.org/en), [Helio](https://helio.exchange/) and [Hubblo](https://hubblo.org/) for supporting the [SoftAWERE](https://gitlab.com/softawere-hackathon/softawere/) tool chain, along with [CodeCarbon](https://codecarbon.io/) contributors.

## Subject

Based on the subject, you will be provided a **training dataset** and an **example code snippet** to guide you in your first submission. The performance of your solution will be assessed using a confidential test dataset. The SoftAWERE Runner server will execute all the submissions and monitor the environmental impacts of your solutions. For a more in-depth understanding of its workings, please refer to the [official documentation](https://softawere-hackathon.gitlab.io/documentation/).

To setup your project and make your first submission follow the guide: [:material-flag: Getting Started](getting_started.md)

## Useful links

[:material-flag: Getting Started](getting_started.md)

[:material-trophy: Leaderboard](http://leaderboard.greenaihack.org/)

[:material-slack: Slack group](https://join.slack.com/t/greenaihack/shared_invite/zt-2umhs28bm-ZXM8jV74vyVs_tNiEgZepQ)

## Rules

1. You can use any model architecture to solve this task (traditional ML, neural networks, symbolic methods).
2. You can use any pretrained model found on the internet **ONLY IF** they are **NOT** trained on a sentiment analysis task (you must fine-tune it yourself).
3. You must only use the provided dataset to train or fine-tune your model on the sentiment analysis task.
4. ou can use any dataset found on the internet for classical pretraining and language modeling tasks (e.g. using wikitext to train an embedding model on general english is allowed).
5. Any attempt to cheat will result in immediate disqualification.
